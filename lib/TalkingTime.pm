package TalkingTime;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::JSON qw(decode_json encode_json);

# This method will run once at server start
sub startup {
    my $self = shift;

    # Load configuration from hash returned by "my_app.conf"
    my $config = $self->plugin('Config');

    # Documentation browser under "/perldoc"
    $self->plugin('PODRenderer') if $config->{perldoc};

    my $channels = {};

    # Router
    my $r = $self->routes;

    $r->get('/' =>
        sub ($c) {
            $c->render(
                template => 'index',
                format   => 'html'
            );
        }
    );

    $r->get('/jsep/talking-time' =>
        sub ($c) {
            $c->render(
                template => 'jsep/talking-time',
                format   => 'js'
            );
        }
    );

    $r->websocket('/w/:channel' =>
        sub ($c) {
            my $channel = $c->param('channel');

            $channels->{$channel}->{tx}->{sprintf("%s", $c->tx)} = $c->tx;
            $channels->{$channel}->{currentList}                 = [] unless defined $channels->{$channel}->{currentList};

            $c->inactivity_timeout(30000000);

            $c->on(
                message => sub ($c, $msg) {
                    my $json = decode_json($msg);
                    if ($json->{newcomer}) {
                        $c->app->log->info($c->dumper($json->{newcomer}));
                        $c->send(
                            encode_json({
                                currentList  => $channels->{$channel}->{currentList}
                            })
                        );
                    } else {
                        $channels->{$channel}->{currentList}  = $json->{currentList} if $json->{currentList};

                        push @{$channels->{$channel}->{currentList}},  $json->{add}  if $json->{add};

                        for my $tx (keys %{$channels->{$channel}->{tx}}) {
                            $channels->{$channel}->{tx}->{$tx}->send($msg);
                        }
                    }
                }
            );
        }
    )->name('ws');
}

1;
